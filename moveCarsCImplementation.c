//  Nathan Suh
//  C implementation of the move cars function in the BML package.
#include <stdlib.h>

//  All of the parameters are integer vectors.  
//  Highly important to NOT index outside of bounds - won't segfault and terrible things will happen.
//  redIndex only has length of how many red cars there are.
//  redval is complete redval column we want to manipulate.  Has length xy
void moveCarsCFast(int* xLen, int* yLen, int* redIndex, int* blueIndex, 
	int* redIndexLength, int* blueIndexLength,
	int* redval, int* blueval, int* redvalFinal, int* bluevalFinal){
	int xy = (*xLen) * (*yLen);
	for(int i = 0; i < *redIndexLength; i++){ // Move red cars
		if(redIndex[i] + (*yLen) <= xy){  //If car is not at the edge
			if((redval[redIndex[i] + (*yLen) - 1]) || (blueval[redIndex[i] + (*yLen) - 1])){
				//Car is blocked, so do nothing.
			} else{  //  Car is free to move.  Move car
				redval[redIndex[i] - 1] = 0;
				redval[redIndex[i] + (*yLen) -1] = 1;
			}
		} else{  // car is at the edge - wrap around like a torus.
			if((redval[redIndex[i] + (*yLen) - xy -1]) || (blueval[redIndex[i] + (*yLen) - xy -1])){
				//position in wrapped around location is blocked
			} else{  //free to move via wrap around.
				redval[redIndex[i]-1] = 0;
				redval[redIndex[i] + (*yLen) - xy-1] = 1;
			}
		}
	}

	for(int i = 0; i < *blueIndexLength; i++){  //Move blue cars
		if(blueIndex[i]%(*yLen) != 0){  //If blue car is not at the edge
			if(redval[blueIndex[i] + 0] || blueval[blueIndex[i] + 0]){  //check one unit up
				//blocked
			} else{  //Free to move, move car.
				blueval[blueIndex[i] -1 ] = 0;
				blueval[blueIndex[i] + 0] = 1;
			}
		} else{  //car is at edge.  wrap around like a torus.
			if(redval[blueIndex[i] - (*yLen) + 0] || blueval[blueIndex[i] - (*yLen) + 0]){ //check wrap around
				//wrap around space is blocked.
			} else{  //Free to wrap around.
				blueval[blueIndex[i] - 1] = 0;
				blueval[blueIndex[i] - (*yLen) +  0] = 1;
			}
		}
	}
	for(int i = 0; i < xy; i++){
		redvalFinal[i] = redval[i];
		bluevalFinal[i] = blueval[i];
	}
}
