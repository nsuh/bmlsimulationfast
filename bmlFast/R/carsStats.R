carsStats <-
function(cars1, cars2, n){ #n is number of cars each color - the number of red and blue cars must be equal.
  n = n*2 #total number of cars
  moved = sum(cars1$redval != cars2$redval)/2 + sum(cars1$blueval != cars2$blueval)/2 #how many cars moved
  blocked = n - moved
  avgVelocity = moved/n
  return (myStats = list(moved = moved, blocked = blocked, avgVelocity = avgVelocity))
}
